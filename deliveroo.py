import random

class bcolors:
    OKCYAN = '\033[96m'
    ENDC = '\033[0m'

MINUTES = [x for x in range(0, 60)]
HOURS = [x for x in range(0, 24)]
DAYS_OF_MONTH = [x for x in range(1, 32)]
DAYS_OF_WEEK = [x for x in range(1, 8)]
MONTHS = [x for x in range(1, 13)]
DAYS_OF_WEEK_MAP = {1: "SUNDAY", 2: "MONDAY", 3: "TUESDAY", 4: "WEDNESDAY", 5: "THURSDAY", 6:"FRIDAY", 7:"SATURDAY"}

# TODO accept literals in place of numbers for month and day
# TODO asserts to be added to verify values are correct

def cron_parser(expression):
    splits = expression.split(" ")

    assert len(splits) == 6

    command = splits[-1]
    minutes = process_regular_expression(splits[0], MINUTES)
    hours = process_regular_expression(splits[1], HOURS)
    day_of_month = process_day_of_month(splits[2], DAYS_OF_MONTH)
    months = process_regular_expression(splits[3], MONTHS)
    day_of_week = process_day_of_week(splits[4], DAYS_OF_WEEK)


    print (bcolors.OKCYAN + "***************INPUT**********************" + bcolors.ENDC)
    print(bcolors.OKCYAN + expression + bcolors.ENDC)
    print (bcolors.OKCYAN + "*************************************" + bcolors.ENDC)
    print("minute ", minutes)
    print("hour ", hours)
    print("day of month ", day_of_month)
    print("month ", months)
    print("day of week ", day_of_week)
    print("command ", command)



def process_regular_expression(expression, all_possible_values):
    if ',' in expression:
        return [int(x) for x in expression.split(",")]
    if '-' in expression:
        splits = expression.split("-")
        a,b = int(splits[0]), int(splits[1])
        return list(range(a, b+1))
    if "/" in expression:
        splits = expression.split("/")
        b = int(splits[1])
        if splits[0] == "*" or splits[0] == "0":
            return [x for x in all_possible_values if x % b == 0]
        else:
            a = int(splits[0])
            ans = []
            i = a
            while i <= all_possible_values[-1]:
                ans.append(i)
                i += b
            return ans
    if "*" in expression:
        return all_possible_values
    
    return expression

def process_regular_day(expression, all_possible_values):
    if "?" in expression:
        return all_possible_values
    if "C" in expression:
        day = int(expression.split("C")[0])
        index = all_possible_values.index(day)
        if index + 1 >= len(all_possible_values):
            return all_possible_values[0]
        return all_possible_values[index + 1]
    
    return process_regular_expression(expression, all_possible_values) 

def process_day_of_month(expression, all_possible_values):
    if "W" in expression:
        val = expression.split("C")[0]
        if val == "L":
            day = 31
            day_of_week = get_random_day_of_the_week_assignment()

            while day_of_week == 5 or day_of_week == 6:
                day_of_week = get_random_day_of_the_week_assignment()
                day -= 1
            
            return day

        day = int(val)
        day_of_week = get_random_day_of_the_week_assignment()
        if day_of_week == 5: # saturday
            if day - 1 == 0:
                return day + 2
            return day - 1
        if day_of_week == 6: # sunday
            if day + 1 > DAYS_OF_MONTH[-1]:
                return day - 2
        return day

    if "L" in expression:
        return all_possible_values[-1]
    return process_regular_day(expression, all_possible_values)

def process_day_of_week(expression, all_possible_values):
    if "#" in expression:
        splits = expression.split("#")
    
        day_of_week = int(splits[0])
        number = int(splits[1])
        order = "first" if number == 1 else "second" if number == 2 else "third" if number == 3 else "fourth" if number == 4 else "fifth" if number == 5 else "undefined"

        return order + " " + DAYS_OF_WEEK_MAP[day_of_week] + " of the month"
    if "L" in expression:
        splits = expression.split("L")
    
        day_of_week = int(splits[0])
        return  "Last " + DAYS_OF_WEEK_MAP[day_of_week] + " of the month"
    return process_regular_day(expression, all_possible_values)


def get_random_day_of_the_week_assignment():
    # TODO: need year to determine if a particular date in a calendar year
    #if a weekday or weekend
    
    r = random.randint(0, 6)
    return r

cron_parser("*/15 0 1,15 * 1-5 /usr/bin/find")
cron_parser("0 12 * * ? /usr/bin/find")
cron_parser("15 10 ? * * /usr/bin/find")
cron_parser("15 10 * * ? /usr/bin/find")
cron_parser("15 10 * * ? /usr/bin/find")
cron_parser("15 10 * * ? /usr/bin/find")
cron_parser("* 14 * * ? /usr/bin/find")
cron_parser("0/5 14 * * ? /usr/bin/find")
cron_parser("0/5 14,18 * * ? /usr/bin/find")
cron_parser("0-5 14 * * ? /usr/bin/find")
cron_parser("10,44 14 ? 3 4 /usr/bin/find")
cron_parser("15 10 ? * 2-6 /usr/bin/find")
cron_parser("15 10 15 * ? /usr/bin/find")
cron_parser("15 10 L * ? /usr/bin/find")
cron_parser("15 10 ? * 6L /usr/bin/find")
cron_parser("15 10 ? * 6#3 /usr/bin/find")
cron_parser("0 12 1/5 * ? /usr/bin/find")
cron_parser("11 11 11 11 ? /usr/bin/find")
cron_parser("0/5 * * * ? /usr/bin/find")
cron_parser("30 10-13 ? * 4-6 /usr/bin/find")
cron_parser("0/30 8-9 5,20 * ? /usr/bin/find")