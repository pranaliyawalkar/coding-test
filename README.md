# README #

### What is this repository for? ###

Take home interview tests code.

### Running deliveroo.py ###

Run the code with 20 built in tests: 

<div class="codehilite"><pre><code># python3 deliveroo.py </code></pre></div>

To add custom tests, call **cron_parser** function with your cron expression:
<div class="codehilite"><pre><code> cron_parser("0 12 * * ? /usr/bin/find") </code></pre></div>


### Contribution guidelines ###

https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm

http://www.brightxpress.com/user/appendix/cron_expressions.htm

For understanding the various cron symbols to support, and for tests

### Who do I talk to? ###

* pranali.yawalkar@gmail.com